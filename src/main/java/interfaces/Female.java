package interfaces;

import java.time.LocalDate;
import java.time.Period;

//class corresponding to "Interfejsy 1"
public class Female implements Human{
	private String birthdate;
	private double height;
	private double weight;
	private String name;
	private static final String sex = "female";
	
	public Female(String birthdate, double height, double weight, String name){
		this.birthdate = birthdate;
		this.height = height;
		this.weight = weight;
		this.name = name;
	}
	
	@Override
	public String getAge() {
		LocalDate birthday = LocalDate.parse(this.birthdate);
		Period age = Period.between(birthday, LocalDate.now());
		String old = Integer.toString(age.getYears()) + " years " + Integer.toString(age.getMonths()) + " months " + Integer.toString(age.getDays()) + " days";
		return old;
	}
	@Override
	public String getName() {
		return this.name;
	}
	@Override
	public double getHeight() {
		return this.height;
	}
	@Override
	public double getWeight() {
		return this.weight;
	}
	@Override
	public boolean isMale() {
		return false;
	}

}
