package interfaces;

//class correspondig to "Interfejsy 2"
public class Triangle implements Figure{

	private double side1;
	private double side2;
	private double side3;
	private double height;
	
	public Triangle(double side1, double side2, double side3, double height){
		this.side1 = side1;
		this.side2 = side2;
		this.side3 = side3;
		this.height = height;
		
	}
	
	public Triangle(double side1, double side2){
		this.side1 = side1;
		this.side2 = side2;
		this.side3 = getHypotenuse();
		this.height = side2;
		
	}
	
	@Override
	public double getPerimeter() {
		return side1 + side2 + side3;
	}

	@Override
	public double getArea() {
		return (side1 * height) / 2;
	}
	
	public double getHypotenuse(){
		return Math.sqrt((Math.pow(side1, 2) + Math.pow(side2, 2)));
	}

}
