package interfaces;

//class correspondig to "Interfejsy 2"
public class Circle implements Figure{
	
	private double radius;
	
	public Circle(double radius){
		this.radius = radius;
	}

	@Override
	public double getPerimeter() {
		return (2 * Math.PI * this.radius);
	}

	@Override
	public double getArea() {
		return ((this.radius * this.radius)* Math.PI);
	}

}
