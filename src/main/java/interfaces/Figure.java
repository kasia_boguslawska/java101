package interfaces;

//interface correspondig to "Interfejsy 2"
public interface Figure {
	
	public double getPerimeter();
	public double getArea();
}
