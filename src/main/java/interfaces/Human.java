package interfaces;

//interface corresponding to "Interfejsy 1"
public interface Human {

	public String getAge();
	public String getName();
	public double getHeight();
	public double getWeight();
	public boolean isMale();
}
