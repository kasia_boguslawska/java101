package interfaces;

import loops.UserInput;

public class FiguresManagement {
	
	public static Circle prepareCircle(){
		String radius = UserInput.getUserInput("Type in the radius of the circle: ");
		return new Circle(Double.valueOf(radius));
	}
	
	public static Rectangle prepareRectangle(){
		String sides = UserInput.getUserInput("Type in the side of the rectangle separated with a semi-colon\nE.g. 4.5;9.0");
		String[] sidesIsolated = sides.split(";");
		return new Rectangle(Double.valueOf(sidesIsolated[0]), Double.valueOf(sidesIsolated[1]));
	}

	public static Triangle prepareTriangle(){
		String legs = UserInput.getUserInput("Type in the legs of the right triangle separated with a semi-clon\nE.g. 16.2;9.99");
		String[] legsIsolated = legs.split(";");
		return new Triangle(Double.valueOf(legsIsolated[0]), Double.valueOf(legsIsolated[1]));
	}
	
	public static Object[] putIntoTable(){
		Object [] figuresTable = new Object[3];
		figuresTable[0] = (Object)prepareCircle();
		figuresTable[1] = (Object)prepareTriangle();
		figuresTable[2] = (Object)prepareRectangle();
		return figuresTable;
	}
	
	public static void showMeasurementsInTable(){
		Object[] figuresTable = putIntoTable();
		for (int i = 0; i < figuresTable.length; i++){
			if(figuresTable[i] instanceof Circle){
				Circle tmpC = (Circle)figuresTable[i];
				System.out.println("Area " + tmpC.getArea() + " perimeter: " + tmpC.getPerimeter());
			} 
			else if (figuresTable[i] instanceof Rectangle){
				Rectangle tmpR = (Rectangle)figuresTable[i];
				System.out.println("Area " + tmpR.getArea() + " perimeter: " + tmpR.getPerimeter());
			}
			else if (figuresTable[i] instanceof Triangle){
				Triangle tmpT = (Triangle)figuresTable[i];
				System.out.println("Area " + tmpT.getArea() + " perimeter: " + tmpT.getPerimeter());
			}
		}
	}
}
