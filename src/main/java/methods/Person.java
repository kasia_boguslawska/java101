package methods;

import java.time.LocalDate;
import java.time.Period;


public class Person {

	private String name;
	private String bday;
	
	
	public Person(String name, String bday) {
		this.name = name;
		this.bday = bday;
	}


	//method corresponding to: "Metody w języku Java 2"
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getbday() {
		return bday;
	}


	public void setbday(String bday) {
		this.bday = bday;
	}
	
	//method corresponding to: "Metody w języku Java 1"
	public String getAge(){
		
		LocalDate birthday = LocalDate.parse(bday);
		Period age = Period.between(birthday, LocalDate.now());
		String old = Integer.toString(age.getYears()) + " years " + Integer.toString(age.getMonths()) + " months " + Integer.toString(age.getDays()) + " days";
		return old;
	}
	

}
