package methods;

public interface Calculations {

	public double calc(double arg1, double arg2, String sign);
	public boolean isEven(int arg1);
	public boolean isDivisibleBy35(int arg1);
	public double power3(double arg1);
	public double root2(double arg1);
	public boolean okForRightTriangle(double arg1, double arg2, double arg3);
}
