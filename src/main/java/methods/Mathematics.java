package methods;
import java.lang.Math;

public class Mathematics implements Calculations{

	public Mathematics() {
	}
	
	//method corresponding to: "Metody w języku Java 3"
	@Override
	public double calc(double arg1, double arg2, String sign) {
		switch(sign){
		case "+":
			return (arg1 + arg2);
		case "-":
			return (arg1 - arg2);
		case "*":
			return (arg1 * arg2);
		default: 
			return -0.0;
			
		}
	}

	//method corresponding to: "Metody w języku Java 4"
	@Override
	public boolean isEven(int arg1) {
		if(arg1 % 2 == 0){
		return true;}
		else {
			return false;
		}
	}

	//method corresponding to: "Metody w języku Java 5"
	@Override
	public boolean isDivisibleBy35(int arg1) {
		if((arg1 % 3 == 0) && (arg1 % 5 == 0)){
		return true;}
		else {
			return false;
		}
	}

	//method corresponding to: "Metody w języku Java 6"
	@Override
	public double power3(double arg1) {
		return arg1 * arg1 * arg1;
	}

	//method corresponding to: "Metody w języku Java 7"
	@Override
	public double root2(double arg1) {
		return Math.sqrt(arg1);
	}

	//method corresponding to: "Metody w języku Java 8"
	@Override
	public boolean okForRightTriangle(double arg1, double arg2, double arg3) {
		double[] sides = {arg1, arg2, arg3};
		double currentlyHighest = Double.MIN_VALUE;
		for (int i = 0; i < sides.length; i++){
			if(sides[i] > currentlyHighest){
				currentlyHighest = sides[i];
			}
			java.util.Arrays.asList(sides).indexOf(currentlyHighest);
		}
		return false;
	}

}
