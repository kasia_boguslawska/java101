package packages_and_objects;

//class corresponding to: "Obiekty i pakiety 1"
public class Human {

	private int age;
	private double height;
	private int weight;
	private String name;
	private Sexes sex;
	
	public Human (int age, double height, int weight, String name, Sexes sex){
		this.age = age;
		this. height = height;
		this.weight = weight;
		this.name = name;
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public double getHeight() {
		return height;
	}

	public int getWeight() {
		return weight;
	}

	public String getName() {
		return name;
	}
	
	public Sexes getGender() {
		return sex;
	}
	
	public boolean isMale(){
		if(this.sex == Sexes.MALE){
			return true;			
		}
		else{
			return false;
		}
	}
}
