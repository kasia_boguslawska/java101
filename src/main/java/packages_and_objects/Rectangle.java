package packages_and_objects;

//class corresponding to: "Obiekty i pakiety 2"
public class Rectangle {

	private double side1;
	private double side2;
	
	public Rectangle(double side1, double side2){
		this.side1 = side1;
		this.side2 = side2;
	}
	
	public double getArea(){
		return this.side1 * this.side2;
	}
	
	public double getPerimeter(){
		return ((this.side1 * 2) + (this.side2 * 2));
	}
	
	public double getDiagonal(){
		return Math.sqrt(Math.pow(side1, 2.0) + Math.pow(side2, 2.0));
	}
}
