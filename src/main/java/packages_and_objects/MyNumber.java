package packages_and_objects;

//class corresponding to: "Obiekty i pakiety 3"
public class MyNumber {
	
	private double number;
	
	public MyNumber(double number){
		this.number = number;
	}
	
	public double getNumber(){
		return number;
	}
	
	public void setNumber(double number){
		this.number = number;
	}
	
	public boolean isOdd(){
		if((this.getNumber() % 2) == 0){
		return false;
		}
		else{
			return true;
		}
	}
	
	public boolean isEven(){
		if(this.isOdd()){
			return false;
		}
		else {
			return true;
		}
	}
	
	public double sqrt(){
		return Math.sqrt(this.getNumber());
	}
	
	public double pow(MyNumber nmbr){
		return Math.pow(this.getNumber(), nmbr.getNumber());
	}
	
	public MyNumber add(MyNumber nmbr){
		this.setNumber(this.getNumber() + nmbr.getNumber());
		return this;
	}
	
	public MyNumber subtract(MyNumber nmbr){
		this.setNumber(this.getNumber() - nmbr.getNumber());
		return this;
	}

	public String toString(){
		Double tmp = this.getNumber();
		return tmp.toString();
	}
}
