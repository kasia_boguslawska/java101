package arrays;

import java.util.Arrays;

import loops.UserInput;

public class ArrayBuilder {

	//method corresponding to: "Tablice 1"
	public static String [] getAlpahbetTable() {
		final String [] first5Alphabet = {"a", "b", "c", "d", "e"};
		return first5Alphabet;
	}
	
	//method corresponding to: "Tablice 2"
	public static double[] getReversedTable(double [] tableInOrder){
		double [] tableReversed = new double[tableInOrder.length];
		for(int i = 0; i < tableReversed.length; i++){
			tableReversed[i] = tableInOrder[tableInOrder.length-1 - i];
		}
		return tableReversed;
	}
	
	public static char[] getReversedText(char [] tableInOrder){
		char [] tableReversed = new char[tableInOrder.length];
		for(int i = 0; i < tableReversed.length; i++){
			tableReversed[i] = tableInOrder[tableInOrder.length-1 - i];
		}
		return tableReversed;
	}
	
	//method corresponding to: "Pętle i instrukcje warunkowe 7"
	public static boolean isPalindrome(){
		char[] originalPhrase = UserInput.getObjectAsCharArray("Type in a word/ phrase: ");
		char [] reversedPhrase = ArrayBuilder.getReversedText(originalPhrase);
		if(Arrays.equals(originalPhrase,reversedPhrase)){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static boolean isPalindrome(char [] originalPhrase){
		char [] reversedPhrase = ArrayBuilder.getReversedText(originalPhrase);
		if(Arrays.equals(originalPhrase,reversedPhrase)){
			return true;
			}
		return false;
	}
}
