package primitive_types;

import packages_and_objects.Human;
import packages_and_objects.Sexes;

//class corresponding to "Typy proste 1"
public class HumanBigTypes {
	
	private Integer age;
	private Double height;
	private Integer weight;
	private String name;
	private Sexes sex;
	
	public HumanBigTypes(Human human) {
		
		this.age = Integer.valueOf(human.getAge());
		this.height = Double.valueOf(human.getHeight());
		this.weight = Integer.valueOf(human.getWeight());
		this.name = human.getName();
		this.sex = human.getGender();
	}
}
