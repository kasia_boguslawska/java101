package primitive_types;

import java.util.Arrays;

import arrays.ArrayBuilder;
import loops.ArrayFactory;
import loops.UserInput;

public class PrimitiveTables {

	public static int [] getSortedNTable(){
		
		int counter = UserInput.getAnyNumber("Tell me how big the array's going to be: ");
		int [] userNumbers = new int [counter];
		for (int i = 0; i < counter; i++){
			userNumbers[i] = UserInput.getAnyNumber("Give me the number to put in the table");
		}
		Arrays.sort(userNumbers);
			return userNumbers;
	}
	
	//method corresponding to "Typy proste 3"
	public static void showSortedNTable(){
		System.out.println(Arrays.toString(PrimitiveTables.getSortedNTable()));
	}
	
	
	//method corresponding to "Typy proste 4"
	public static void stringOperations(){

		char[] userString = UserInput.getObjectAsCharArray("Give me a word to play with: ");
		System.out.println("The word is " + userString.length + " letters long. Is it a palindrome? " + ArrayBuilder.isPalindrome(userString));
		ArrayFactory.backToForth(userString);
		
	}
}
