package loops;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserInput {
	
	public static String getUserInput(String text){
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println(text);
		try {
			 return reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "Obtaining user input failed";
	}

	public static int getAnyNumber(String text) {
		String dataAsText = UserInput.getUserInput(text);
		int forOperations = Integer.valueOf(dataAsText);
		return forOperations;
	}

	public static Object getPositiveNumber(String text) {
		int forOperations = (int) UserInput.getAnyNumber(text);
		if (forOperations >= 0) {
			return forOperations;
		} else {
			System.out.println("This is a negative value");
			return null;
		}
	}

	public static char [] getObjectAsCharArray(String text) {
		String objectAsText = UserInput.getUserInput(text);
		return objectAsText.toCharArray();
		}
}
