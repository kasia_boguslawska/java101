package loops;

import java.util.Arrays;

import arrays.ArrayBuilder;
import interfaces.Circle;
import interfaces.Female;
import interfaces.FiguresManagement;
import interfaces.Male;
import interfaces.Rectangle;
import interfaces.Triangle;
import primitive_types.PrimitiveTables;

public class Application {

	public static void main(String[] args) {
		
		/*
		System.out.println(Arrays.toString(PrimitiveTables.getSortedNTable()));
		System.out.println(ArrayBuilder.isPalindrome());
		PrimitiveTables.stringOperations();

		
		System.out.println(Arrays.toString(ArrayBuilder.getReversedText(UserInput.getObjectAsCharArray("Give me a word: "))));
		FiguresManagement.showMeasurementsInTable();
		
		Triangle right = new Triangle(3.0, 4.0);
		System.out.println(right.getPerimeter());
		
		Rectangle rect = new Rectangle(5.4, 99.156);
		System.out.println(rect.getArea());
		
		Circle circle = new Circle(7.56);
		System.out.println(circle.getPerimeter());
		
		Circle crl1 = FiguresManagement.prepareCircle();
		Rectangle rect1 = FiguresManagement.prepareRectangle();
		Triangle right1 = FiguresManagement.prepareTriangle();
		
		System.out.println(crl1.getArea());
		System.out.println(rect1.getArea());
		System.out.println(right1.getArea());
	
		String command = "Give me a number you'll be working with: ";
		int myNumber = (int)UserInput.getPositiveNumber(command);
		int otherNumber = (int)UserInput.getPositiveNumber(command);
		NumbersOperations.displayNumbersTill(myNumber);
		System.out.println();
		NumbersOperations.displayNumbersWhile(otherNumber);
		
		
		System.out.println(Arrays.toString(ArrayBuilder.getAlpahbetTable()));
		double[] tableNice = {1.0, 87.56, 9.14, 0.436, 19.0};
		double [] tableNotNice = ArrayBuilder.getReversedTable(tableNice);
		System.out.println(Arrays.toString(tableNotNice));
		
		String sth1 = "Give me a number you'll be working with: ";
		int nmbrSth = (int)UserInput.getPositiveNumber(sth1);
		System.out.println(nmbrSth);
		
		System.out.println();
		ArrayFactory test = new ArrayFactory();
		test.build1DTable();
		
		System.out.println();
		ArrayFactory.backToForth();
		ArrayFactory.backToForthString();
		System.out.println(ArrayBuilder.isPalindrome());
		
		Female she = new Female("1989-01-02", 1.7, 65.2, "She");
		Male he = new Male ("1980-04-12", 1.8, 85.0, "He");
		
		System.out.println(he.getAge());
		System.out.println(she.isMale());
		*/
	}

}
