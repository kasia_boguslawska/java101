package loops;

import java.util.Arrays;
import java.util.Random;

//class corresponding to: "Pętle i instrukcje warunkowe 3"
public class ArrayFactory {

	private int dimension;

	public ArrayFactory() {
		this.dimension = (int) UserInput.getPositiveNumber("What's the array size? ");
	}

	//method corresponding to: "Pętle i instrukcje warunkowe 3.1"
	public int[] build1DTable() {

		int[] table1 = new int [this.dimension];
		return table1;
		
	}

	//method corresponding to: "Pętle i instrukcje warunkowe 3.2"
	public int[][] build2DTable() {
		int[][] table2 = new int [this.dimension][this.dimension];
		return table2;
	}

	//method corresponding to: "Pętle i instrukcje warunkowe 4"
	public static void backToForth(){
		char [] objectAsArray = UserInput.getObjectAsCharArray("Give me a number, ideally a long one ");
		for (int i = objectAsArray.length-1; i >= 0; i--){
			System.out.println(objectAsArray[i]);
		}
	}
	
	public static void backToForth(char [] stringAsArray){
		for (int i = stringAsArray.length-1; i >= 0; i--){
			System.out.print(stringAsArray[i]);
		}
	}
	
	//method corresponding to: "Pętle i instrukcje warunkowe 5"
	public static void backToForthString(){
		char [] objectAsArray = UserInput.getObjectAsCharArray("Type in some text: ");
		for (int i = objectAsArray.length-1; i >= 0; i--){
			System.out.print(objectAsArray[i]);
		}
	}
	
	//method corresponding to: "Pętle i instruckje warunkowe 8"
	public static int [][] getIdMatrix(){
		int matrixSize = (int) UserInput.getPositiveNumber("Please provide the size of the matix: ");
		int [][] matrix = new int [matrixSize][matrixSize];
		for (int i = 0; i < matrixSize; i++){
			matrix [i][i] = 1;
		}
		return matrix;
	}
	//method corresponding to: "Pętle i instrukcje warunkowe 9"
	public static void createAndShowTable(){
		ArrayFactory array = new ArrayFactory();
		int[] table = array.build1DTable();
		for (int i = 0; i < table.length; i++){
			table[i] = new Random().nextInt(800);
		}
		System.out.println(Arrays.toString(table));
	}
	
	//method corresponding to: "Pętle i instrukcje warunkowe 10"
	public static int [] createAndSortTable(){
		ArrayFactory array = new ArrayFactory();
		int[] table = array.build1DTable();
		for (int i = 0; i < table.length; i++){
			table[i] = new Random().nextInt(800);
		}
		Arrays.sort(table);
		return table;
	}
}
