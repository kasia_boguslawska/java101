package loops;

public class NumbersOperations {
	
	//method corresponding to: "Pętle i instrukcje warunkowe 1"
	public static void displayNumbersTill(int value){
		for (int i = 0; i <= value; i++){
			System.out.print(i + "; ");
		}
	}
	
	//method corresponding to: "Pętle i instrukcje warunkowe 2"
	public static void displayNumbersWhile(int value){
		int count = 0;
		while (count <= value){
			System.out.print(count + "; ");
			count++;
		}
	}
	
	//method corresponding to: "Pętle i instrukcje warunkowe 6" and "Typy proste 2"
	public static String getBinaryRepresentation(){
		Integer forBinary = (Integer)UserInput.getAnyNumber("Type in the number you wish to see as its binary representation ");
		String binary = Integer.toBinaryString(forBinary);
		return binary;
	}

}
